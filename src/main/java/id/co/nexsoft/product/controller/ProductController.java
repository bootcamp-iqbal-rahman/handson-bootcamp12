package id.co.nexsoft.product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.product.model.Product;
import id.co.nexsoft.product.repository.ProductRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/product")
    public List<Product> getAllData() {
        return productRepository.findAll();
    }

    @GetMapping("/product/{id}")
    public Product getDataById(@PathVariable int id) {
        return productRepository.findById(id);
    }

    @PostMapping("/product")
    @ResponseStatus(HttpStatus.CREATED)
    public Product addData(@RequestBody Product product) {
        
        return productRepository.save(product);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteData(@PathVariable int id) {
        productRepository.deleteById(id);
    }
}